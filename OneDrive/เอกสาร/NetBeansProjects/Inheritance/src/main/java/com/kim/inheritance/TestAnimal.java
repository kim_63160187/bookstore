/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.inheritance;

/**
 *
 * @author ASUS
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dee = new Dog("Dee","Black&White");
        dee.speak();
        dee.walk();
        
        Cat timy = new Cat("Timy","Orange");
        timy.speak();
        timy.walk();
        
        Duck leo = new Duck("Leo","Yellow");
        leo.speak();
        leo.walk();
        leo.fly();
        
        System.out.println("Leo is Animal: " + (leo instanceof Animal));
        System.out.println("Leo is Duck: " + (leo instanceof Duck));
        System.out.println("Leo is Cat: " + (leo instanceof Object));
        System.out.println("Animal is Dog:" + (animal instanceof Dog));
        System.out.println("Animal is Animal:" + (animal instanceof Animal));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = leo;
        ani2 = timy;
        
        System.out.println("Ani1: leo is Duck " + (ani1 instanceof Duck));
        
        Animal[] animals = {dee, timy, leo};
        for(int i=0; i< animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
